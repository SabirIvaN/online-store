<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'ProductsController@index', 'as' => 'Products']);

Route::get('products', ['uses' => 'ProductsController@index', 'as' => 'Products']);

Route::get('products/men', ['uses' => 'ProductsController@menProducts', 'as' => 'MenProducts']);

Route::get('products/women', ['uses' => 'ProductsController@womenProducts', 'as' => 'WomenProducts']);

Route::get('search', ['uses' => 'ProductsController@search', 'as' => 'SearchProducts']);

Route::get('products/addToCart/{id}', ['uses' => 'ProductsController@addProductToCart', 'as' => 'AddToCartProduct']);

Route::get('cart', ['uses' => 'ProductsController@showCart', 'as' => 'CartProducts']);

Route::get('products/deleteItemFromCart/{id}', ['uses' => 'ProductsController@deleteItemFromCart', 'as' => 'DeleteItemFromCart']);

Route::get('products/checkoutProducts/', ['uses' => 'ProductsController@checkoutProducts', 'as' => 'CheckoutProducts']);

Route::get('products/createNewOrder/', ['uses' => 'ProductsController@createNewOrder', 'as' => 'CreateNewOrder']);

Route::get('products/createOrder/', ['uses' => 'ProductsController@createOrder', 'as' => 'CreateOrder']);

Route::get('payment/paymentPage', ['uses' => 'Payment\PaymentsController@showPaymentPage', 'as' => 'ShowPaymentPage']);

Route::get('payment/paymentReceipt/{paymentID}/{payerID}', ['uses' => 'Payment\PaymentsController@showPaymentReceipt', 'as' => 'ShowPaymentReceipt']);

Route::get('product/increasingSingleProduct/{id}', ['uses' => 'ProductsController@increaseSingleProduct', 'as' => 'IncreaseSingleProduct']);

Route::get('product/decreaseSingleProduct/{id}', ['uses' => 'ProductsController@decreaseSingleProduct', 'as' => 'DecreaseSingleProduct']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['restrictToAdmin']], function () {
    Route::get('admin/editProductForm/{id}', ['uses' => 'Admin\AdminProductsController@editProductForm', 'as' => 'AdminEditProductForm']);

    Route::get('admin/editProductImageForm/{id}', ['uses' => 'Admin\AdminProductsController@editProductImageForm', 'as' => 'AdminEditProductImageForm']);

    Route::post('admin/updateProductImage/{id}', ['uses' => 'Admin\AdminProductsController@updateProductImage', 'as' => 'AdminUpdateProductImage']);

    Route::post('admin/updateProduct/{id}', ['uses' => 'Admin\AdminProductsController@updateProduct', 'as' => 'AdminUpdateProduct']);

    Route::get('admin/createProductForm/', ['uses' => 'Admin\AdminProductsController@createProductForm', 'as' => 'AdminCreateProductForm']);

    Route::post('admin/sendCreateProductForm/', ['uses' => 'Admin\AdminProductsController@sendCreateProductForm', 'as' => 'AdminSendCreateProductForm']);

    Route::get('admin/deleteProduct/{id}', ['uses' => 'Admin\AdminProductsController@deleteProduct', 'as' => 'AdminDeleteProduct']);

    Route::get('admin/ordersPanel', ['uses' => 'Admin\AdminProductsController@ordersPanel', 'as' => 'OrdersPanel']);

    Route::get('admin/deleteOrder/{id}', ['uses' => 'Admin\AdminProductsController@deleteOrder', 'as' => 'AdminDeleteOrder']);

    Route::get('payment/getPaymentInfoByOrderId/{order_id}', ['uses' => 'Payment\PaymentsController@getPaymentInfoByOrderId', 'as' => 'GetPaymentInfoByOrderId']);

    Route::get('admin/editOrderForm/{order_id}', ['uses' => 'Admin\AdminProductsController@editOrderForm', 'as' => 'AdminEditOrderForm']);

    Route::post('admin/updateOrder/{order_id}', ['uses' => 'Admin\AdminProductsController@updateOrder', 'as' => 'AdminUpdateOrder']);
});

Route::get('admin/products', ['uses' => 'Admin\AdminProductsController@index', 'as' => 'AdminDisplayProducts'])->middleware('restrictToAdmin');

Route::get('/session', function(\Illuminate\Http\Request $request) {
    dd($request->session()->all());
});

Route::post('products/addToCartAjaxPost', ['uses' => 'ProductsController@addToCartAjaxPost', 'as' => 'AddToCartAjaxPost']);

Route::get('products/addToCartAjaxGet/{id}', ['uses' => 'ProductsController@addToCartAjaxGet', 'as' => 'AddToCartAjaxGet']);
