@extends('layouts.admin')

@section('body')

@if(Auth::user()->admin_level == 1)
<div class="table-responsive">

    <form action="/admin/updateProduct/{{$product->id}}" method="post">

        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="{{$product->name}}" required>
        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="description" value="{{$product->description}}" required>
        </div>


        <div class="form-group">
            <label for="type">Тип</label>
            <input type="text" class="form-control" name="type" id="type" placeholder="type" value="{{$product->type}}" required>
        </div>

        <div class="form-group">
            <label for="type">Цена</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="price" value="{{$product->price}}" required>
        </div>
        <button type="submit" name="submit" class="btn btn-default">Редактировать</button>
    </form>

</div>
@else
<div class="alert alert-danger">Только администраторы первого уровня могут редактировать товары!</div>
@endif

@endsection
