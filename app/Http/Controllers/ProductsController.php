<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Mail\OrderCreatedEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index() {
        $products = Product::paginate(3);
        return view('products', compact('products'));
    }

    public function menProducts() {
        $products = DB::table('products')->where('type', 'Мужчины')->get();
        return view('menProducts', compact('products'));
    }

    public function womenProducts() {
        $products = DB::table('products')->where('type', 'Женщины')->get();
        return view('womenProducts', compact('products'));
    }

    public function search(Request $request) {
        $searchText = $request->get('searchText');
        $products = Product::where('name', 'Like', $searchText . '%')->paginate(3);
        return view('products', compact('products'));
    }

    public function addProductToCart(Request $request, $id) {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addItem($id, $product);
        $request->session()->put('cart', $cart);
        return redirect()->route('Products');
    }

    public function showCart() {
        $cart = Session::get('cart');
        if ($cart) {
            return view('cart', ['cartItems' => $cart]);
        } else {
            return redirect()->route('Products');
        }
    }

    public function deleteItemFromCart(Request $request, $id) {
        $cart = $request->session()->get('cart');
        if (array_key_exists($id, $cart->items)) {
            unset($cart->items[$id]);
        }
        $prevCart = $request->session()->get('cart');
        $updatedCart = new Cart($prevCart);
        $updatedCart->updatePriceAndQuantity();
        $request->session()->put('cart', $updatedCart);
        return redirect()->route('CartProducts');
    }

    public function increaseSingleProduct(Request $request, $id) {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addItem($id, $product);
        $request->session()->put('cart', $cart);
        return redirect()->route('CartProducts');
    }

    public function decreaseSingleProduct(Request $request, $id) {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        if ($cart->items[$id]['quantity'] > 1) {
            $product = Product::find($id);
            $cart->items[$id]['quantity'] = $cart->items[$id]['quantity'] - 1;
            $cart->items[$id]['totalSinglePrice'] = $cart->items[$id]['quantity'] * $product['price'];
            $cart->updatePriceAndQuantity();
            $request->session()->put('cart', $cart);
        }
        return redirect()->route('CartProducts');
    }

    public function checkoutProducts() {
        return view('checkoutProducts');
    }

    public function createNewOrder(Request $request) {
        $cart  = Session::get('cart');
        $firstName = $request->input('first_name');
        $address = $request->input('address');
        $lastName = $request->input('last_name');
        $zip = $request->input('zip');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $isUserLoggedIn = Auth::check();
        if ($isUserLoggedIn) {
            $user_id = Auth::id();
        } else {
            $user_id = 0;
        }
        if ($cart) {
            $date = date('Y-m-d H:i:s');
            $newOrderArray = [
                'status' => 'on_hold',
                'date' => $date,
                'del_date' => $date,
                'price' => $cart->totalPrice,
                'first_name' => $firstName,
                'address' => $address,
                'last_name' => $lastName,
                'zip' => $zip,
                'phone' => $phone,
                'email' => $email,
                'user_id' => $user_id
            ];
            $createdOrder = DB::table('orders')->insert($newOrderArray);
            $orderId = DB::getPdo()->lastInsertId();
            foreach ($cart->items as $cartItem) {
                $itemId = $cartItem['data']['id'];
                $itemName = $cartItem['data']['name'];
                $itemPrice = $cartItem['data']['price'];
                $newItemsInCurrentOrder = [
                    'item_id' => $itemId,
                    'order_id' => $orderId,
                    'item_name' => $itemName,
                    'item_price' => $itemPrice
                ];
                $createdOrderItems = DB::table('order_items')->insert($newItemsInCurrentOrder);
            }
            Session::forget('cart');
            $paymentInfo = $newOrderArray;
            $paymentInfo['order_id'] = $orderId;
            $request->session()->put('payment_info', $paymentInfo);
            return redirect()->route('ShowPaymentPage');
        } else {
            return redirect()->route('Products');
        }
    }

    public function createOrder() {
        $cart = Session::get('cart');
        if ($cart) {
            $date = date('Y-m-d H:i:s');
            $newOrderArray = ['status' => 'on_hold', 'date' => $date, 'del_date' => $date, 'price' => $cart->totalPrice];
            $createdOrder = DB::table('orders')->insert($newOrderArray);
            $orderId = DB::getPdo()->lastInsertId();
            foreach ($cart->items as $cartItem) {
                $itemId = $cartItem['data']['id'];
                $itemName = $cartItem['data']['name'];
                $itemPrice = $cartItem['data']['price'];
                $newItemsCurrentOrder = ['item_id' => $itemId, 'order_id' => $orderId, 'item_name' =>$itemName, 'item_price' => $itemPrice];
                $createdOrderItems = DB::table('order_items')->insert($newItemsCurrentOrder);
            }
            Session::forget('cart');
            Session::flush();
            return redirect()->route('Products')->withsuccess('Thanks for choosing us.');
        } else {
            return redirect()->route('Products');
        }
    }

    private function sendMail() {
        $user = Auth::user();
        $cart = Session::get('cart');
        if ($cart != null && $user != null) {
            Mail::to($user)->send(new OrderCreatedMail($cart));
        }
    }

    public function addToCartAjaxPost(Request $request) {
        $id = $request->input('id');
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addItem($id,$product);
        $request->session()->put('cart', $cart);
        return response()->json(['totalQuantity', $cart->totalQuantity]);
    }

    public function addToCartAjaxGet(Request $request, $id) {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);
        $product = Product::find($id);
        $cart->addItem($id,$product);
        $request->session()->put('cart', $cart);
        return response()->json(['totalQuantity', $cart->totalQuantity]);
    }
}
