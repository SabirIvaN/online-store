<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Домашняя страница | Магазин</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png') }}">
    <script src="{{ asset('js/jquery.js') }}"></script>
  </head>
  <!--/head-->

  <body>
    <header id="header">
      <!--header-->
      <div class="header_top">
        <!--header_top-->
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <div class="contactinfo">
                <ul class="nav nav-pills">
                  <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                  <li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="social-icons pull-right">
                <ul class="nav navbar-nav">
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header_top-->

      <div class="header-middle">
        <!--header-middle-->
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="logo pull-left">
                <a href="index.html"><img src="{{ asset('images/home/logo.png') }}" alt="" /></a>
              </div>
              <div class="btn-group pull-right">
                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                    Россия
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Украина</a></li>
                    <li><a href="#">Казахстан</a></li>
                  </ul>
                </div>

                <div class="btn-group">
                  <button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
                    Российский рубль
                    <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#">Украинская гривна</a></li>
                    <li><a href="#">Тенге</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-sm-8">
              <div class="shop-menu pull-right">
                <ul class="nav navbar-nav">
                  <li><a href="#"><i class="fa fa-user"></i> Аккаунт</a></li>
                  <li><a href="#"><i class="fa fa-star"></i> Список желаний</a></li>
                  <li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Проверка</a></li>

                  <li>
                      <a href="{{ route('CartProducts') }}">
                          <i class="fa fa-shopping-cart"></i>
                          @if(Session::has('cart'))
                          <span id="totalQuantity" class="cart-with-numbers">{{ Session::get('cart')->totalQuantity }}</span>
                          @endif
                          Корзина</a>
                  </li>

                  @if(Auth::check())
                  <li><a href="/home"><i class="fa fa-lock"></i> Профиль</a></li>
                  @else
                  <li><a href="/login"><i class="fa fa-lock"></i> Войти</a></li>
                  @endif

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header-middle-->

      <div class="header-bottom">
        <!--header-bottom-->
        <div class="container">
          <div class="row">
            <div class="col-sm-9">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Навигация</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="mainmenu pull-left">
                <ul class="nav navbar-nav collapse navbar-collapse">
                  <li><a href="index.html" class="active">Дом</a></li>
                  <li class="dropdown"><a href="#">Магазин<i class="fa fa-angle-down"></i></a>
                    <ul role="menu" class="sub-menu">
                      <li><a href="shop.html">Товары</a></li>
                      <li><a href="product-details.html">Детали товаров</a></li>
                      <li><a href="checkout.html">Проверка</a></li>
                      <li><a href="cart.html">Корзина</a></li>
                      <li><a href="login.html">Логин</a></li>
                    </ul>
                  </li>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="search_box pull-right">
                  <form action="search" method="GET">
                      <input type="text" name="searchText" placeholder="Поиск" />
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--/header-bottom-->
    </header>
    <!--/header-->
