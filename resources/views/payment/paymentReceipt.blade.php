@extends('layouts.index')

@section('center')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Главная</a></li>
                <li class="active">Корзизна покупок</li>
            </ol>
        </div>
        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-12 clearfix" style="margin-bottom:20px">
                    <div class="bill-to">
                        <p> Получение оплаты</p>
                        <div class="form-one">
                            <h1 class="text-center"> Спасибо за то что выбрали наши товары!</h1>
                            <div class="total_area">
                                <ul>
                                    <li>ID заказа<span>{{ $payment_receipt['order_id'] }}</span></li>
                                    <li>ID плательщика<span>{{ $payment_receipt['paypal_payer_id'] }}</span></li>
                                    <li>ID платежа<span>{{ $payment_receipt['paypal_payment_id'] }}</span></li>
                                    <li>Результат<span id="amount">{{ $payment_receipt['price'] }}</span></li>
                                </ul>
                                <a class="btn btn-default update" href="{{route('Products')}}">Shop Again!</a>
                            </div>
                        </div>
                        <div class="form-two"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!--/#payment-->
@endsection
