@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Панель управления</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Имя: {!! Auth::user()->name !!}</p>
                    <p>E-Mail: {!! Auth::user()->email !!}</p>
                    <a href="{{ route('Products') }}" class="btn btn-primary">Главная страница</a>
                    @if($userData->isAdmin())
                    <a href="{{ route('AdminDisplayProducts') }}" class="btn btn-primary">Панель администратора</a>
                    @else
                    <div class="btn btn-primary">Вы не являетесь администратором!</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
