@extends('layouts.admin')

@section('body')
    @if(Auth::user()->admin_level == 1)
    <div class="table-responsive">
        <form action="{{ route('AdminUpdateOrder', ['order_id' => $order->order_id]) }}" method="post">

            {{ csrf_field() }}

            <div class="form-group">
                <label for="date">Дата заказа</label>
                <input type="date" class="form-control" name="date" id="date" placeholder="Date" value="{{ $order->date }}" required>
            </div>
            <div class="form-group">
                <label for="del_date">Дата доставки</label>
                <input type="text" class="form-control" name="del_date" id="del_date" placeholder="Delivery date" value="{{ $order->del_date }}" required>
            </div>
            <div class="form-group">
                <label for="price">Цена</label>
                <input type="number" step="0.1" class="form-control" name="price" id="price" placeholder="Price" value="{{ $order->price }}" required>
            </div>
            <div class="form-group">
                <label for="status">Статус</label>
                <input type="text" class="form-control" name="status" id="status" placeholder="Price" value="{{ $order->status }}" required>
            </div>
            <button type="submit" name="submit" class="btn btn-default">Редактировать</button>
        </form>
    </div>
    @else
    <div class="alert alert-danger">Только администраторы первого уровня могут редактировать товары!</div>
    @endif
@endsection
