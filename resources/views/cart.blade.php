@extends('layouts.index')

@section('center')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Главная</a></li>
              <li class="active">Корзина покупок</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Элемент</td>
                        <td class="description"></td>
                        <td class="price">Цена</td>
                        <td class="quantity">Количество</td>
                        <td class="total">Результат</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>

                    @foreach($cartItems->items as $item)
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="{{ Storage::disk('local')->url('product-images/' . $item['data']['image']) }}" alt=""></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{{ $item['data']['name'] }}</a></h4>
                            <p>{{ $item['data']['description'] }} - {{ $item['data']['type'] }}</p>
                            <p>ID: {{ $item['data']['id'] }}</p>
                        </td>
                        <td class="cart_price">
                            <p>₽{{ $item['data']['price'] }}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href="{{ route('IncreaseSingleProduct', ['id' => $item['data']['id']]) }}"> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{ $item['quantity'] }}" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href="{{ route('DecreaseSingleProduct', ['id' => $item['data']['id']]) }}"> - </a>
                            </div>
                        </td>
                        <td class="cart_total">
                            <p class="cart_total_price">₽{{ $item['totalSinglePrice'] }}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{ route('DeleteItemFromCart', ['id' => $item['data']['id']]) }}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</section> <!--/#cart_items-->

<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>Что Вы хотели бы сделать дальше?</h3>
            <p>Выберите если у Вас есть дисконтная карта или купон.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Используйте купон</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Сделайте подарок</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Стоимость доставки и налогов</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Страна:</label>
                            <select>
                                <option>Россия</option>
                                <option>Украина</option>
                                <option>Казахстан</option>
                            </select>

                        </li>
                        <li class="single_field">
                            <label>Регион / Облась:</label>
                            <select>
                                <option>Москва/Московская область</option>
                                <option>Санкт-Петербург/Ленинградская область</option>
                                <option>Республика Татарстан</option>
                                <option>Ульяновская область</option>
                                <option>Волгоградская область</option>
                                <option>Еврейская Автономная Область</option>
                                <option>Республика Башкортостан</option>
                                <option>Республика Карелия</option>
                            </select>

                        </li>
                        <li class="single_field zip-field">
                            <label>Почтовый индекс:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Получить цитату</a>
                    <a class="btn btn-default check_out" href="">Продолжить</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    <ul>
                        <li>Количество <span>{{ $cartItems->totalQuantity }}</span></li>
                        <li>Стоимость доставки <span>Бесплатно</span></li>
                        <li>Результат <span>₽{{ $cartItems->totalPrice }}</span></li>
                    </ul>
                        <a class="btn btn-default update" href="">Обновить</a>
                        <a class="btn btn-default check_out" href="{{ route('CheckoutProducts') }}">Оплатить</a>
                </div>
            </div>
        </div>
    </div>
</section><!--/#do_action-->
@endsection
