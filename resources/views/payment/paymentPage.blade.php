@extends('layouts.index')

@section('center')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Главная</a></li>
                <li class="active">Корзина покупок</li>
            </ol>
        </div>
        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="bill-to">
                        <p> Купить/посчитаь</p>
                        <div class="form-one">
                           <div class="total_area">
                               <ul>
                                   <li>Статус оплаты
                                       @if($payment_info['status'] == 'on_hold')
                                       <span>пока не оплачен</span>
                                       @endif
                                   </li>
                                   <li>Доставка <span>Бесплатно</span></li>
                                   <li>Результат <span>₽{{ $payment_info['price'] }}</span></li>
                               </ul>
                               <a class="btn btn-default update" href="">Обновить</a>
                               <a class="btn btn-default check_out" id="paypal-button">Оплатить сейчас</a>
                           </div>
                       </div>
                       <div class="form-two">
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section> <!--/#payment-->
@endsection

<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
    env: 'sandbox',
    client: {
      sandbox: 'AXIHpOz5E7gFbQnT_KWod_yESO74dXs7tdXg5mvAl1zLfB6LMMzNqZ5qykK92Oz5ZL2GViT_kqRHc0zP',
      production: 'demo_production_client_id'
    },
    locale: 'en_US',
    style: {
      size: 'small',
      color: 'gold',
      shape: 'pill',
    },
    commit: true,
    payment: function(data, actions) {
      return actions.payment.create({
        transactions: [{
          amount: {
            total: "{{ $payment_info['price'] }}",
            currency: 'RUB'
          }
        }]
      });
    },
    onAuthorize: function(data, actions) {
      return actions.payment.execute().then(function() {
        window.alert('Спасибо за покупку!');
        window.location = './paymentReceipt/' + data.paymentID + '/' + data.payerID;
      });
    }
  }, '#paypal-button');
</script>
