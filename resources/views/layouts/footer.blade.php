        <footer id="footer">
        <!--Footer-->
            <div class="footer-top">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-2">
                      <div class="companyinfo">
                        <h2><span>М</span>агазин</h2>
                        <p>Но действия представителей оппозиции являются только методом политического участия и рассмотрены исключительно в разрезе маркетинговых и финансовых предпосылок.</p>
                      </div>
                    </div>
                    <div class="col-sm-7">
                      <div class="col-sm-3">
                        <div class="video-gallery text-center">
                          <a href="#">
                            <div class="iframe-img">
                              <img src="{{ asset('images/home/iframe1.png') }}" alt="" />
                            </div>
                            <div class="overlay-icon">
                              <i class="fa fa-play-circle-o"></i>
                            </div>
                          </a>
                          <p>Акция</p>
                          <h2>24 декабря 2020</h2>
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="video-gallery text-center">
                          <a href="#">
                            <div class="iframe-img">
                              <img src="{{ asset('images/home/iframe2.png') }}" alt="" />
                            </div>
                            <div class="overlay-icon">
                              <i class="fa fa-play-circle-o"></i>
                            </div>
                          </a>
                          <p>Акция</p>
                          <h2>24 декабря 2020</h2>
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="video-gallery text-center">
                          <a href="#">
                            <div class="iframe-img">
                              <img src="{{ asset('images/home/iframe3.png') }}" alt="" />
                            </div>
                            <div class="overlay-icon">
                              <i class="fa fa-play-circle-o"></i>
                            </div>
                          </a>
                          <p>Акция</p>
                          <h2>24 декабря 2020</h2>
                        </div>
                      </div>

                      <div class="col-sm-3">
                        <div class="video-gallery text-center">
                          <a href="#">
                            <div class="iframe-img">
                              <img src="{{ asset('images/home/iframe4.png') }}" alt="" />
                            </div>
                            <div class="overlay-icon">
                              <i class="fa fa-play-circle-o"></i>
                            </div>
                          </a>
                          <p>Акция</p>
                          <h2>24 декабря 2014</h2>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="address">
                        <img src="{{ asset('images/home/map.png') }}" alt="" />
                        <p>Россия, Санкт-Петербург, Университетский Политехнический Колледж</p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="footer-widget">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-2">
                      <div class="single-widget">
                        <h2>Услуги</h2>
                        <ul class="nav nav-pills nav-stacked">
                          <li><a href="#">Помощь</a></li>
                          <li><a href="#">Свяжитесь с нами</a></li>
                          <li><a href="#">Статус заказа</a></li>
                          <li><a href="#">Изменить геолокацию</a></li>
                          <li><a href="#">Ответы на вопросы</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="single-widget">
                        <h2>Категории</h2>
                        <ul class="nav nav-pills nav-stacked">
                          <li><a href="#">Футболки</a></li>
                          <li><a href="#">Мужчины</a></li>
                          <li><a href="#">Женщины</a></li>
                          <li><a href="#">Подарочные карты</a></li>
                          <li><a href="#">Ботинки</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="single-widget">
                        <h2>Политика</h2>
                        <ul class="nav nav-pills nav-stacked">
                          <li><a href="#">Условия использования</a></li>
                          <li><a href="#">Политика приватность</a></li>
                          <li><a href="#">Политика возврата средств</a></li>
                          <li><a href="#">Счетная система</a></li>
                          <li><a href="#">Билетная система</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="single-widget">
                        <h2>О магазине</h2>
                        <ul class="nav nav-pills nav-stacked">
                          <li><a href="#">Информационная компания</a></li>
                          <li><a href="#">Карьера</a></li>
                          <li><a href="#">Местоположение магазина</a></li>
                          <li><a href="#">Программа помощи</a></li>
                          <li><a href="#">Авторское право</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-sm-3 col-sm-offset-1">
                      <div class="single-widget">
                        <h2>О магазине</h2>
                        <form action="#" class="searchform">
                          <input type="text" placeholder="Ваш E-Mail адрес" />
                          <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                          <p>Получать актуальную информацию от нас</p>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            </div>

            <div class="footer-bottom">
                <div class="container">
                  <div class="row">
                    <p class="pull-left">Copyright © 2013 Магазин. Все права защищены.</p>
                    <p class="pull-right">Дизайн спроектирован <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
                  </div>
                </div>
            </div>

        </footer>
        <!--/Footer-->

        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.scrollUp.min.js') }}"></script>
        <script src="{{ asset('js/price-range.js') }}"></script>
        <script src="{{ asset('js/jquery.prettyPhoto.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>
