<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

class AdminProductsController extends Controller
{
    public function index() {
        $products = Product::paginate(3);
        return view('admin.displayProducts', ['products' => $products]);
    }

    public function createProductForm() {
        return view('admin.createProductForm');
    }

    public function sendCreateProductForm(Request $request) {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $type = $request->input('type');
        Validator::make($request->all(), ['image' => 'required|image|mimes:jpg,jpeg,png|max:5000'])->validate();
        $ext = $request->file('image')->getClientOriginalExtension();
        $stringImageReFormat = str_replace(' ', '', $request->input('name'));
        $imageName = $stringImageReFormat . '.' . $ext;
        $imageEncoded = File ::get($request->image);
        Storage::disk('local')->put('public/product-images/' . $imageName, $imageEncoded);
        $newProductArray = ['name' => $name, 'description' => $description, 'image' => $imageName, 'price' => $price, 'type' => $type];
        $created = DB::table('products')->insert($newProductArray);
        if ($created) {
            return redirect()->route('AdminDisplayProducts');
        } else {
            return 'Product was not created!';
        }
    }

    public function editProductForm($id) {
        $product = Product::find($id);
        return view('admin.editProductForm', ['product' => $product]);
    }

    public function editProductImageForm($id) {
        $product = Product::find($id);
        return view('admin.editProductImageForm', ['product' => $product]);
    }

    public function updateProductImage(Request $request, $id) {
        Validator::make($request->all(), ['image' => 'required|image|mimes:jpg,jpeg,png|max:5000'])->validate();

        if ($request->hasFile('image')) {
            $product = Product::find($id);
            $exists = Storage::disk('local')->exists('public/product-images/' . $product->image);
            if ($exists) {
                Storage::delete('public/product-images/' . $product->image);
            }
            $ext = $request->file('image')->getClientOriginalExtension();
            $request->image->storeAs('public/product-images/', $product->image);
            $arrayToUpdate = ['image' => $product->image];
            DB::table('products')->where('id', $id)->update($arrayToUpdate);
            return redirect()->route('AdminDisplayProducts');
        } else {
            $error = 'No image was selected!';
            return $error;
        }
    }

    public function updateProduct(Request $request, $id) {
        $name = $request->input('name');
        $description = $request->input('description');
        $price = $request->input('price');
        $type = $request->input('type');
        $updateToArray = ['name' => $name, 'description' => $description, 'price' => $price, 'type' => $type];
        DB::table('products')->where('id', $id)->update($updateToArray);
        return redirect()->route('AdminDisplayProducts');
    }

    public function deleteProduct($id) {
        $product = Product::find($id);
        $exists = Storage::disk('local')->exists('public/product-images/' . $product->image);
        if ($exists) {
            Storage::delete('public/product-images/' . $product->image);
        }
        Product::destroy($id);
        return redirect()->route('AdminDisplayProducts');
    }

    public function ordersPanel() {
        $orders = DB::table('orders')->paginate(10);
        return view('admin.ordersPanel', ['orders' => $orders]);
    }

    public function deleteOrder(Request $request, $id) {
        $deleted = DB::table('orders')->where('order_id', $id)->delete();
        if ($deleted) {
            return redirect()->back()->with('orderDeletionStatus', 'Order ' . $id . ' was successfully deleted!');
        } else {
            return redirect()->back()->with('orderDeletionStatus', 'Order ' . $id . ' was not deleted!');
        }
    }

    public function editOrderForm($order_id) {
        $order = DB::table('orders')->where('order_id', $order_id)->get();
        return view('admin.editOrderForm', ['order' => $order[0]]);
    }

    public function updateOrder(Request $request, $order_id) {
        $date = $request->input('date');
        $del_date = $request->input('del_date');
        $status = $request->input('status');
        $price = $request->input('price');
        $updateArray = ['date' => $date, 'del_date' => $del_date, 'status' => $status, 'price' => $price];
        DB::table('orders')->where('order_id', $order_id)->update($updateArray);
        return redirect()->route('OrdersPanel');
    }
}
