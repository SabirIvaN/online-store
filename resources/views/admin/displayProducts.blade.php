@extends('layouts.admin')

@section('body')
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Изображение</th>
            <th>Название</th>
            <th>Описание</th>
            <th>Тип</th>
            <th>Цена</th>
            <th>Редактировать изображение</th>
            <th>Редактировать</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tbody>

        @foreach($products as $product)
        <tr>
            <td>{{$product['id']}}</td>
            <td><img src="{{ Storage::url('product-images/' . $product['image'])}}" alt="{{ Storage::url($product['image']) }}" width="100" height="100" style="max-height:220px" ></td>
            <td>{{$product['name']}}</td>
            <td>{{$product['description']}}</td>
            <td>{{$product['type']}}</td>
            <td>₽{{$product['price']}}</td>
            <td><a href="{{ route('AdminEditProductImageForm', ['id' => $product['id']]) }}" class="btn btn-primary">Редактировать изображение</a></td>
            <td><a href="{{ route('AdminEditProductForm', ['id' => $product['id']]) }}" class="btn btn-primary">Редактировать</a></td>
            <td><a href="{{ route('AdminDeleteProduct', ['id' => $product['id']]) }}"  class="btn btn-warning">Удалить</a></td>
        </tr>
        @endforeach

        </tbody>
    </table>

    {{$products->links()}}

</div>
@endsection
