<?php

namespace App\Http\Controllers\Payment;

use App\Cart;
use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class PaymentsController extends Controller
{
    public function showPaymentPage() {
        $payment_info = Session::get('payment_info');
        if($payment_info['status'] == 'on_hold') {
            return view('payment.paymentpage',['payment_info'=> $payment_info]);
        } else {
            return redirect()->route("Products");
        }
    }

    private function storePaymentInfo($paypalPaymentID,$paypalPayerID){
        $payment_info = Session::get('payment_info');
        $order_id = $payment_info['order_id'];
        $status = $payment_info['status'];
        $paypal_payment_id = $paypalPaymentID;
        $paypal_payer_id = $paypalPayerID;
        if($status == 'on_hold') {
            $date = date('Y-m-d H:i:s');
            $newPaymentArray = ['order_id' => $order_id, 'date' => $date, 'amount' => $payment_info['price'], 'paypal_payment_id' => $paypal_payment_id, 'paypal_payer_id' => $paypal_payer_id];
            $created_order = DB::table("payments")->insert($newPaymentArray);
            DB::table('orders')->where('order_id', $order_id)->update(['status' => 'paid']);
        }
    }

    public function showPaymentReceipt($paypalPaymentID,$paypalPayerID){
        if(!empty($paypalPaymentID) && !empty($paypalPayerID)) {
            $this->validatePayment($paypalPaymentID,$paypalPayerID);
            $this->storePaymentInfo($paypalPaymentID,$paypalPayerID);
            $payment_receipt = Session::get('payment_info');
            $payment_receipt['paypal_payment_id'] = $paypalPaymentID;
            $payment_receipt['paypal_payer_id'] = $paypalPayerID;
            Session::forget("payment_info");
            return view('payment.paymentReceipt',['payment_receipt' => $payment_receipt]);
        } else {
            return redirect()->route("Products");
        }
    }

    private function validatePayment($paypalPaymentID, $paypalPayerID){
        $paypalEnv = 'sb-adxqb1280896@business.example.com';
        $paypalURL = 'https://api.sandbox.paypal.com/v1/';
        $paypalClientID = 'AXIHpOz5E7gFbQnT_KWod_yESO74dXs7tdXg5mvAl1zLfB6LMMzNqZ5qykK92Oz5ZL2GViT_kqRHc0zP';
        $paypalSecret = 'EElAreFxAzvhKdrry6RHlJKM-xMUOt8695jvWC-jfWLmrV2dxcLh817U36Nr8AzW84-xt2FYweETTYGK';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $paypalURL . 'oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $paypalClientID . ':' . $paypalSecret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        $response = curl_exec($ch);
        curl_close($ch);
        if(empty($response)) {
            return false;
        } else {
            $jsonData = json_decode($response);
            $curl = curl_init($paypalURL . 'payments/payment/' . $paypalPaymentID);
            curl_setopt($curl, CURLOPT_POST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $jsonData->access_token, 'Accept: application/json', 'Content-Type: application/xml']);
            $response = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($response);
            return $result;
        }
    }

    public function showCart(){
        $cart = Session::get('cart');
        if($cart) {
            return view('cart',['cartItems'=> $cart]);
        } else {
            return redirect()->route("Products");
        }
    }

    public function checkoutProducts(){
        return view('checkoutProducts');
    }

    public function createNewOrder(Request $request){
        $cart = Session::get('cart');
        $first_name = $request->input('first_name');
        $address = $request->input('address');
        $last_name = $request->input('last_name');
        $zip = $request->input('zip');
        $phone = $request->input('phone');
        $email = $request->input('email');
        if($cart) {
            $date = date('Y-m-d H:i:s');
            $newOrderArray = ['status' => 'on_hold', 'date' => $date, 'del_date' => $date, 'price' => $cart->totalPrice, 'first_name' => $first_name, 'address' => $address, 'last_name'=>$last_name, 'zip'=>$zip,'email'=>$email,'phone'=>$phone];
            $created_order = DB::table("orders")->insert($newOrderArray);
            $order_id = DB::getPdo()->lastInsertId();
            foreach ($cart->items as $cart_item){
                $item_id = $cart_item['data']['id'];
                $item_name = $cart_item['data']['name'];
                $item_price = $cart_item['data']['price'];
                $newItemsInCurrentOrder = ['item_id' => $item_id, 'order_id' => $order_id, 'item_name' => $item_name, 'item_price' => $item_price];
                $created_order_items = DB::table("order_items")->insert($newItemsInCurrentOrder);
            }
            Session::forget("cart");
            return redirect()->route("Products")->withsuccess("Thanks for choosing us");
        } else {
            return redirect()->route("Products");
        }
    }

    public function getPaymentInfoByOrderId($order_id){
        $paymentInfo = DB::table('payments')->where('order_id', $order_id)->get();
        return json_encode($paymentInfo[0]);
    }
}
