@extends('layouts.admin')

@section('body')
<script src="{{ asset('js/jquery.js') }}"></script>
<style>
    .payment-window {
        display: none;
        position: fixed;
        z-index: 1;
        padding-top: 100px;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        background-color: rgb(0, 0, 0);
        background-color: rgba(0, 0, 0, 0.4);
    }

    .payment-window-content {
        background-color: #fefefe;
        margin: auto;
        padding: 30px;
        border: 1px solid #888888;
        width: 45%;
    }

    .payment-window-close {
        color: #aaaaaa;
        float: right;
        margin-left: 20px;
        font-size: 28px;
        font-weight: bold;
    }

    .payment-window-close:hover,
    .payment-window-close:focus {
        color: #aaaaaa;
        text-decoration: none;
        cursor: pointer;
    }
</style>
<h1>Панель заказов</h1>
@if(session('orderDeletionStatus'))
<div class="alert alert-danger">{{ session('orderDeletionStatus') }}</div>
@endif
<div class="table-responsive">
    <table class="table table-striped">
      <thead>
          <tr>
              <th>ID</th>
              <th>Дата заказа</th>
              <th>Дата доставки</th>
              <th>Цена</th>
              <th>ID пользователя</th>
              <th>Статус</th>
              <th>Редактировать</th>
              <th>Удалить</th>
          </tr>
      </thead>
      <tbody>
          @foreach($orders as $order)
          <tr>
              <td>{{ $order->order_id }}</td>
              <td>{{ $order->date }}</td>
              <td>{{ $order->del_date }}</td>
              <td>{{ $order->price }}</td>
              <td>{{ $order->user_id }}</td>
              <td>{{ $order->status }}</td>
              <td><a class="payment-info-button btn btn-success" onclick="getPaymentInfo('{{ $order->order_id }}', '{{ $order->status }}')">Payment info</a></td>
              <td><a href="{{ route('AdminEditOrderForm', ['order_id' => $order->order_id]) }}" class="btn btn-primary">Edit</a></td>
              <td><a href="{{ route('AdminDeleteOrder', ['id' => $order->order_id]) }}" class="btn btn-warning" onclick="return confirm('Are you sure you want to delete this order?')">Remove</a></td>
          </tr>
          @endforeach
      </tbody>
    </table>

    <div class="payment-window" id="my-payment-window">
        <div class="payment-window-content">
            <span class="payment-window-close">&times;</span>
            <h2>Платежная информация</h2>
            <p>Загрузка..</p>
            <p></p>
            <p></p>
            <p></p>
            <p></p>
        </div>
    </div>

    {{ $orders->links() }}
</div>
<script type="text/javascript">
    function getPaymentInfo(order_id, status) {
        if (status == 'paid') {
            $.get('http://127.0.0.1:8000/payment/getPaymentInfoByOrderId/' + order_id, function(data) {
                var paymentInfo = JSON.parse(data);
                $('.payment-window').show();
                $('.payment-window-content p:eq(0)').text('ID: ' + paymentInfo.id);
                $('.payment-window-content p:eq(1)').text('Payment ID: ' + PaymentInfo.paypal_payment_id);
                $('.payment-window-content p:eq(2)').text('Payer ID: ' + paymentInfo.paypal_payer_id);
                $('.payment-window-content p:eq(3)').text('Amount $' + paymentInfo.amount);
                $('.payment-window-content p:eq(4)').text('Date: ' + paymentInfo.date);
            });
        } else if (status == 'on_hold') {
            $('.payment-window').show();
            $('.payment-window-content p:eq(0)').text('Not paid yet.');
            $('.payment-window-content p:eq(1)').text('');
            $('.payment-window-content p:eq(2)').text('');
            $('.payment-window-content p:eq(3)').text('');
            $('.payment-window-content p:eq(4)').text('');
        } else {
            $('.payment-window').show();
            $('.payment-window-content p:eq(0)').text('Undefind status.');
            $('.payment-window-content p:eq(1)').text('');
            $('.payment-window-content p:eq(2)').text('');
            $('.payment-window-content p:eq(3)').text('');
            $('.payment-window-content p:eq(4)').text('');
        }
        $('.payment-window-close').click(function() {
            $('.payment-window').hide();
        });
    }
</script>
@endsection
