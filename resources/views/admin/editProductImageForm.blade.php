@extends('layouts.admin')

@section('body')
<div class="table-responsive">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>

            <li>{!! print_r($errors->all()) !!}</li>

        </ul>
    </div>
    @endif

    <h3>Текущее изображение</h3>
    <div><img src="{{ Storage::url('product-images/' . $product['image']) }}" width="100" height="100" style="max-height:220px" ></div>
    <form action="/admin/updateProductImage/{{$product->id}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}

        <div class="form-group">
            <label for="description">Обновить изображение</label>
            <input type="file" class=""  name="image" id="image" placeholder="Image" value="{{$product->image}}" required>
        </div>
        <button type="submit" name="submit" class="btn btn-default">Обновить</button>
    </form>
</div>
@endsection
