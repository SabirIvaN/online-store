<?php

namespace App\Mail;

use App\Cart;
use Illuminate\Bus\Queueable;
use Illuminate\Bus\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $cart;

    public function __construct(Cart $cart)
    {
        $thos->cart = $cart;
    }

    public function build() {
        return $this->view('emails.orderCreatedEmail');
    }
}
