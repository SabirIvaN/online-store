<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id('order_id');
            $table->date('date');
            $table->text('status');
            $table->date('del_date');
            $table->decimal('price', 8, 2);
            $table->text('first_name')->nullable();
            $table->text('address')->nullable();
            $table->string('last_name')->nullable();
            $table->integer('phone')->nullable();
            $table->integer('zip')->nullable();
            $table->string('email')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
