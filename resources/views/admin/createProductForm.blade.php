@extends('layouts.admin')

@section('body')
<div class="table-responsive">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            <li>{!! print_r($errors->all()) !!}</li>
        </ul>
    </div>
    @endif

    <h2>Создать новый товар</h2>
    <form action="/admin/sendCreateProductForm" method="post" enctype="multipart/form-data">

        {{csrf_field()}}

        <div class="form-group">
            <label for="name">Название товара</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Название товара" required>
        </div>
        <div class="form-group">
            <label for="description">Описание</label>
            <input type="text" class="form-control" name="description" id="description" placeholder="Описание" required>
        </div>

        <div class="form-group">
            <label for="image">Изображение</label>
            <input type="file" name="image" id="image" required>
        </div>
        <div class="form-group">
            <label for="type">Тип</label>
            <input type="text" class="form-control" name="type" id="type" placeholder="Тип" required>
        </div>
        <div class="form-group">
            <label for="type">Цена</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Цена" required>
        </div>
        <button type="submit" name="submit" class="btn btn-default">Создать</button>
    </form>
</div>
@endsection
