@extends('layouts.index')

@section('center')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Главная</a></li>
                <li class="active">Корзина покупок</li>
            </ol>
        </div>

        @if(Auth::check())

        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="bill-to">
                        <p> Доставка/Счет</p>
                        <div class="form-one">
                            <form action="{{ route('CreateNewOrder') }}" method="GET">

                                {{csrf_field()}}

                                <input type="text" name="email" placeholder="E-Mail" required>
                                <input type="text" name="first_name" placeholder="Имя" required>
                                <input type="text" name="middle_name" placeholder="Отчество" >
                                <input type="text" name="last_name" placeholder="Фамилия"  required>
                                <input type="text" name="address" placeholder="Адрес 1" required>
                                <input type="text" placeholder="Адрес 2">
                                <input type="text" name="zip" placeholder="Почтовый индекс" required>
                                <select>
                                    <option>Россия</option>
                                    <option>Украина</option>
                                    <option>Казахстан</option>
                                </select>
                                <select>
                                    <option>Москва/Московская область</option>
                                    <option>Санкт-Петербург/Ленинградская область</option>
                                    <option>Республика Татарстан</option>
                                    <option>Ульяновская область</option>
                                    <option>Волгоградская область</option>
                                    <option>Еврейская Автономная Область</option>
                                    <option>Республика Башкортостан</option>
                                    <option>Республика Карелия</option>
                                </select>
                                <input type="text" name="phone" placeholder="Телефон">
                                <input type="text" placeholder="Мобильный телефон">
                                <input type="text" placeholder="Факс">
                                <button class="btn btn-default check_out" type="submit" name="submit" >Выполнить платеж</button>
                            </form>
                        </div>
                        <div class="form-two">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> <!--/#cart_items-->
<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>Что бы Вы хотели сделать следующим?</h3>
            <p>Выберите купон, сделайте подарок.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Использовать купон</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Сделать подарок</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Страна:</label>
                            <select>
                                <option>Россия</option>
                                <option>Украина</option>
                                <option>Казахстан</option>
                            </select>
                        </li>
                        <li class="single_field">
                            <label>Регион/область</label>
                            <select>
                                <option>Москва/Московская область</option>
                                <option>Санкт-Петербург/Ленинградская область</option>
                                <option>Республика Татарстан</option>
                                <option>Ульяновская область</option>
                                <option>Волгоградская область</option>
                                <option>Еврейская Автономная Область</option>
                                <option>Республика Башкортостан</option>
                                <option>Республика Карелия</option>
                            </select>
                        </li>
                        <li class="single_field zip-field">
                            <label>Почтовый код:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Связаться</a>
                    <a class="btn btn-default check_out" href="">Продолжить</a>
                </div>
            </div>
        </div>

      @else

      <div class="alert alert-danger" role="alert">
          <strong>Пожалуйста</strong><a href="{{route('login') }}">войдите</a> или создайте профиль!
      @endif

    </div>
</section><!--/#do_action-->
@endsection
