@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Подтверждение Вашего E-Mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Ссылка для подтверждения Вашего E-Mail отправлена Вам на электронную почту.') }}
                        </div>
                    @endif

                    {{ __('До продолжения, пожалуйста подтвердите Ваш E-Mail.') }}
                    {{ __('Если Вы не подтвердили Ваш E-Mail.') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('нажмите здесь что-бы вернуться') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
